﻿using System;
using CompanyHierarchy.CodeBase;

namespace Ombasa
{
    //4,2,500
    //3,1,800
    //5,1,500
    //1,,1000
    //2,1,500
    class Program
    {
        static void Main(string[] args)
        {
            Hierarchy tree =
                new Hierarchy(new Employee { Id = 1, Salary = 1000 },
                    new Hierarchy(new Employee { Id = 2, Salary = 500 },
                        new Hierarchy(new Employee { Id = 4, Salary = 500 },
                            new Hierarchy(new Employee { Id = 7, Salary = 550 })),
                        new Hierarchy(new Employee { Id = 6, Salary = 500 })),
                    new Hierarchy(new Employee { Id = 3, Salary = 800 }),
                    new Hierarchy(new Employee { Id = 5, Salary = 500 })

                );
            // Traverse and print the tree using Depth-First-Search

            tree.TraverseDFS();
            Console.ReadKey();
        }
    }
}