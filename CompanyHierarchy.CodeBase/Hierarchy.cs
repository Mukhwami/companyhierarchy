﻿using System;
namespace CompanyHierarchy.CodeBase
{
    public class Hierarchy
    {

        private Node manager;

        public Hierarchy(Employee value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(
                    "Employee is not valid!");
            }
            this.manager = new Node(value);

        }
        public Hierarchy(Employee emp, params Hierarchy[] employees) : this(emp)
        {
            foreach (Hierarchy employee in employees)
            {
                this.manager.AddEmployee(employee.manager);
            }
        }
        public Node Manager
        {
            get { return this.manager; }

        }
        private void PrintDFS(Node root, string spaces)
        {
            if (this.manager == null)
            {
                return;
            }
            Console.WriteLine(spaces + root.Value.Id + "(" + spaces + root.Value.Salary+")");

            Node employees = null;
            for (int i = 0; i < root.EmployeeCount; i++)
            {
                employees = root.GetEmployee(i);
                PrintDFS(employees, spaces + " > ");
            }

        }
        public void TraverseDFS()
        {
            this.PrintDFS(this.manager, string.Empty);

        }

    }
}