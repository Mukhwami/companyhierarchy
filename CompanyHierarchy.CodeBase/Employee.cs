﻿namespace CompanyHierarchy.CodeBase
{
    public class Employee
    {
        public int   Id { get; set; }
        public long Salary { get; set; }
        public override string ToString()
        {
            return $"{Id}{Salary}"; 
        }
    }
}