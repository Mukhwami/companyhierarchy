﻿using System;
using System.Collections.Generic;
namespace CompanyHierarchy.CodeBase
{
    public class Node
    {
        private  Employee _Employee;
        private bool hasManager;
        private List<Node> employees;
        public Node(Employee emp)
        {
            if (emp == null)
            {
                throw new ArgumentNullException(
                    "Employee is not valid!");
            }
            this._Employee = emp;
            this.employees = new List<Node>();

        }
        public Employee Value
        {
            get
            {
                return this._Employee;
            }

            set
            {
                this._Employee = value;
            }

        }
        public int EmployeeCount
        {
            get { return this.employees.Count; }

        }

        public void AddEmployee(Node emp)
        {
            if (emp == null)
            {
                throw new ArgumentNullException("Employee is not valid!");

            }
            if (emp.hasManager)
            {
                throw new ArgumentException("The employee has a manager already");
            }
            emp.hasManager = true;
            this.employees.Add(emp);
        }
        public Node GetEmployee(int index)
        {
            return this.employees[index];
        }

    }
}